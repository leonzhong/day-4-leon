package com.afs.tdd.strategy;

import com.afs.tdd.Location;

public class EastDirectStrategy implements DirectStrategy {

    @Override
    public void action(Location location) {
        location.setCoordinationX(location.getCoordinationX() + 1);
    }
}
