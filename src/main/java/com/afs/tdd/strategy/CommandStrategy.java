package com.afs.tdd.strategy;

import com.afs.tdd.Location;

public interface CommandStrategy {
    void action(Location location);
}
