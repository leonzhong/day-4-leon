package com.afs.tdd.strategy;

import com.afs.tdd.Location;

public class NorthDirectStrategy implements DirectStrategy {

    @Override
    public void action(Location location) {
        location.setCoordinationY(location.getCoordinationY()+1);
    }
}
