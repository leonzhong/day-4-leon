package com.afs.tdd.strategy;

import com.afs.tdd.Location;

public class MoveCommandStrategy implements CommandStrategy {
    @Override
    public void action(Location location) {
        location.getDirection().getDirectStrategy().action(location);
    }
}
