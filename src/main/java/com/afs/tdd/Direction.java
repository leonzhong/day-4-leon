package com.afs.tdd;

import com.afs.tdd.strategy.*;

public enum Direction {
    West(new WestDirectStrategy()),
    East(new EastDirectStrategy()),
    South(new SouthDirectStrategy()),
    North(new NorthDirectStrategy());

    private final DirectStrategy directStrategy;

    Direction(DirectStrategy directStrategy) {
        this.directStrategy = directStrategy;
    }

    public DirectStrategy getDirectStrategy() {
        return directStrategy;
    }
}
