package com.afs.tdd;

public class Location {
    private int coordinationX;
    private int coordinationY;
    private Direction direction;

    public Location(int coordinationX, int coordinationY, Direction direction) {
        this.coordinationX = coordinationX;
        this.coordinationY = coordinationY;
        this.direction = direction;
    }

    public int getCoordinationX() {
        return coordinationX;
    }

    public void setCoordinationX(int coordinationX) {
        this.coordinationX = coordinationX;
    }

    public int getCoordinationY() {
        return coordinationY;
    }

    public void setCoordinationY(int coordinationY) {
        this.coordinationY = coordinationY;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }
}
