Objective:

- Learn about unit testing, including an introduction to testing, preparation, operation, and verification.

- Learn about the concept of TDD, write test cases, and perform development validation.


Reflective:

- I feel fulfilled.


Interpretive:

- When writing test cases, it is necessary to understand the requirements and divide the situations, which means that we need to invest time in writing tests and understanding requirements.
- When writing test cases and performing verification, I unconsciously improve the code and cover other cases. I am not sure if this is still considered TDD?
- I feel that I am not very good to express, and I often fail to answer questions directly.

Decision:

- In future tasks, use TDD’s concept to develop.